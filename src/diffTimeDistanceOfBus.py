from pyspark import SparkConf, SparkContext
from pyspark import BasicProfiler
from geopy.distance import vincenty
from datetime import datetime
import time

def distance(origin, destination):
    return vincenty(origin, destination).meters

def valuesObjectAndDate(records):
    date_pattern = '%Y-%m-%dT%H:%M:%S.000Z'
    records_data = []
    for record in records:
        temp = [record[0], record[1], record[2]]
        records_data.append(temp)

    records_data.sort(key=lambda x: time.mktime(time.strptime(x[2],
                                                date_pattern)))

    previousRecord = None
    diffSeconds = 0
    acumulatedSpeed = 0
    average = 0
    for record in records_data:
        if previousRecord == None:
            previousRecord = record
            continue
        previousDate = datetime(*time.strptime(previousRecord[2],
                                               date_pattern)[:-2])
        previousLocation = previousRecord[1]
        currentDistance = distance(previousLocation,record[1])

        date_time = datetime(*time.strptime(record[2], date_pattern)[:-2])

        diffSeconds += (date_time - previousDate).seconds
        acumulatedSpeed += currentDistance/diffSeconds
        average = acumulatedSpeed/(len(records_data)-1)
    return [record[0],average]

class SpeedAverage(BasicProfiler):
    def show(self, id):
        print("My custom profiles for RDD:%s" % id)

    def process(data):
        conf = SparkConf().set("spark.python.profile", "true")
        sc = SparkContext('local', 'test', conf=conf, profiler_cls=SpeedAverage)
        input  = sc.parallelize(data)
        byObject = input.groupBy(lambda record: record[0])
        collections = byObject.mapValues(valuesObjectAndDate).collect()
        return SpeedAverage.parse_to_dict(collections)

    def parse_to_dict(collections):
        results = {}
        for bus, speed_average in collections:
            results[bus] = speed_average[1]
        return results
